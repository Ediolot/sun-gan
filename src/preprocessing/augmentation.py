from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import png
from scipy import io
from scipy.ndimage.interpolation import rotate
from skimage.transform import resize
from tqdm import tqdm

from src.common import ABCD_MINUS, AUGMENTED_PATH, SIMULATIONS_PATH
from src.preprocessing.utils import crop_center, normalize

DOWNSAMPLE_SIZES = [(256, 256), (128, 128), (64, 64), (32, 32), (16, 16), (8, 8), (4, 4)]
AUGMENTED_PATH.mkdir(parents=True, exist_ok=True)
PLOT = False


def augment(img):
    """ Perform augmentation on a single image: 2 flips and 7 rotations. Output 10 images including original. """
    rows, cols = img.shape
    tiled = np.empty((rows * 3, cols * 3))
    for tile_row in range(0, 3):
        for tile_col in range(0, 3):
            begin_row = rows * tile_row
            begin_col = cols * tile_col
            end_row = rows * (tile_row + 1)
            end_col = cols * (tile_col + 1)
            tiled[begin_row:end_row, begin_col:end_col] = img
    return [
        np.copy(img),
        np.copy(np.flip(img, axis=0)),  # Flip returns a view
        np.copy(np.flip(img, axis=1)),  # Flip returns a view
        rotate(img, 90),
        rotate(img, 180),
        rotate(img, 270),
        crop_center(rotate(tiled, 45), rows, cols),
        crop_center(rotate(tiled, 135), rows, cols),
        crop_center(rotate(tiled, 225), rows, cols),
        crop_center(rotate(tiled, 315), rows, cols)
    ]


def save_npy_images(imgs, output_folder: Path):
    mx = np.amax(imgs[0])
    mi = np.amin(imgs[0])
    output_folder.mkdir(parents=True, exist_ok=True)
    for i, img in enumerate(tqdm(imgs, desc='Saving augmented images')):
        file_name = output_folder / (str(i) + '.png')
        normalized = normalize(img, data_range=(mi, mx), output_range=(0, 255))
        normalized = np.clip(normalized, a_min=0.0, a_max=255.0).astype(np.uint8)
        png.from_array(normalized, 'L').save(file_name)


def read_all_simulations():
    imgs = []
    for file in SIMULATIONS_PATH.iterdir():
        if file.is_file() and file.suffix == '.sav':
            imgs += list(io.readsav(file)['cnt'])
    return imgs


def plot(imgs, n=10):
    for i in range(0, n):
        plt.subplot(3, 4, i + 1)
        plt.imshow(imgs[i])
        plt.title('(' + ABCD_MINUS[i] + ')')
        plt.xticks(fontsize=6)
        plt.yticks(fontsize=6)
    plt.show()


def save(imgs, base: Path, name):
    np.save(base / (name + '.npy'), np.stack(imgs))
    save_npy_images(imgs, base / (name + '/0/'))


def main():
    # Read images ------------------------------------------------------------------
    imgs = read_all_simulations()
    assert all(x.shape == imgs[0].shape for x in imgs), 'All input images must have the same size'
    print('{:d} images of size ({:d},{:d})'.format(len(imgs), imgs[0].shape[0], imgs[0].shape[1]))

    # Augment images ---------------------------------------------------------------
    augmented = []
    for img in tqdm(imgs, desc='Augmenting images'):
        augmented += augment(img)

    save(augmented, AUGMENTED_PATH, 'solar_images_augmented')
    if PLOT:
        plot(imgs)

    # Downsample images ------------------------------------------------------------
    for size in DOWNSAMPLE_SIZES:
        txt = '{:d}x{:d}'.format(*size)
        downsampled = [resize(img, size) for img in tqdm(augmented, desc='Downscaling images to ' + txt)]
        save(downsampled, AUGMENTED_PATH, 'solar_images_augmented_downsampled_' + txt)
        if PLOT:
            plot(downsampled)


if __name__ == '__main__':
    main()
