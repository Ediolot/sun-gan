from pathlib import Path

import matplotlib.pyplot as plt
from astropy.io import fits
from mpl_toolkits.axes_grid1 import make_axes_locatable

from src.common import FITS_IN_PATH, FITS_OUT_PATH
from src.preprocessing.augmentation import normalize


def fits_to_png(fits_path: Path, png_path: Path):
    f = fits.open(fits_path)
    im = normalize(f[0].data)
    print(f.info())

    fig, ax = plt.subplots()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)

    ax.axis('off')
    im = ax.imshow(im, cmap='viridis', interpolation='nearest')
    fig.colorbar(im, cax=cax, orientation='vertical')

    plt.savefig(png_path, bbox_inches='tight')
    plt.show()


if __name__ == '__main__':
    fits_to_png(FITS_IN_PATH, FITS_OUT_PATH)
