import math
from itertools import product

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from torchvision.utils import make_grid
from pathlib import Path
import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torch.utils.data import DataLoader


def load_dataset(path: Path, batch_size, shuffle=True, workers=1, return_dataset=False):
    """ Create PyTorch DataLoader from folder. """
    dataset = datasets.ImageFolder(root=str(path),
                                   loader=pil_grayscale_loader,
                                   transform=transforms.Compose([
                                       transforms.ToTensor(),
                                       transforms.Normalize((0.5,), (0.5,)),
                                   ]))
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=workers)
    return dataset, data_loader if return_dataset else data_loader


def normalize(data, data_range=None, output_range=(0, 1)):
    """ Normalize numpy data in range of data_range [min, max], output range may be modified. """
    mi = np.amin(data) if data_range is None else data_range[0]
    mx = np.amax(data) if data_range is None else data_range[1]
    normalized = (data - mi) / (mx - mi)
    return normalized * (output_range[1] - output_range[0]) + output_range[0]


def crop_center(img, rows, cols):
    img_rows, img_cols = img.shape
    center_row = img_rows // 2
    center_col = img_cols // 2
    # Copy so we dont maintain the reference for the big image
    return np.copy(img[(center_row - rows // 2):(center_row + rows // 2),
                       (center_col - cols // 2):(center_col + cols // 2)])


def pil_grayscale_saver(path, image):
    image = np.array(image)
    image = Image.fromarray((np.array(image) * 255).astype(np.uint8))
    image.save(path)


def pil_grayscale_loader(path):
    with open(str(path), 'rb') as f:
        return Image.open(f).convert('L')


def grid_size(k, block_shape=None, square_block=None, window_ratio=1.0):
    """
    Given k blocks of size (rows, cols), find the best grid (rows, cols) to fit the blocks in a window
    of the given ratio
    """
    assert square_block is not None or block_shape is not None

    if k <= 0:
        return 0, 0

    if square_block:
        h, w = 1, 1
    else:
        h, w = block_shape

    n_ = math.sqrt(k * w / (h * window_ratio))  # Rows
    m_ = k / n_  # Cols

    # Iterate the integer neighborhood and find best fit
    n_ = math.floor(n_)
    m_ = math.floor(m_)
    p1 = window_ratio * window_ratio * h / w
    p2 = w / h

    best_area = -1
    best_n = -1
    best_m = -1
    for i, j in product([0, 1], repeat=2):  # Always 4 iterations
        n = n_ + i
        m = m_ + j
        if m * n >= k:
            area = np.min([p1 / m ** 2, p2 / n ** 2])
            if area > best_area:
                best_area = area
                best_n = n
                best_m = m

    return best_n, best_m


def plot_data_loader(data_loader, device):
    """ Plot a batch from the data loader in a grid """
    real_batch = next(iter(data_loader))

    batch_size, _, _, _ = real_batch[0].shape
    rows, cols = grid_size(real_batch[0].shape[0], square_block=True, window_ratio=16/9)

    grid = make_grid(real_batch[0].to(device)[:64], padding=2, normalize=True, nrow=cols).cpu()
    grid = np.transpose(grid, (1, 2, 0))  # Change axis, so channels are last

    plt.figure()
    plt.axis("off")
    plt.title("Training Images")
    plt.imshow(grid)
    plt.show()
