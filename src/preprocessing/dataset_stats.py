import collections

import matplotlib.pyplot as plt
import numpy as np

from src.common import AUGMENTED_PATH
from src.preprocessing.utils import normalize


# Careful, this needs >16GB of RAM recommended !!


def main():
    sizes = {}
    for file in AUGMENTED_PATH.iterdir():
        if file.is_file() and file.suffix == '.npy' and 'solar_images_augmented' in file.name:
            print('Loading ' + str(file), end='...')
            data = normalize(np.load(file))
            print('Done!')
            size = data.shape[1]

            # Original size is 576, in that case, make a center crop
            if size == 576:
                data = normalize(data[:, 32:544, 32:544])
                size = data.shape[1]

            # We intend to plot 512, 128, 32 and 8. Plotting everything would be overkill
            if size not in [512, 128, 32, 8]:
                continue

            # Store samples of some augmentations and original in a dictionary to use later
            sizes[size] = [data[0], data[2], data[7], data[5]]

    # Sort to plot images in order
    sizes = collections.OrderedDict(reversed(sorted(sizes.items())))

    # Plot different image downsamples for different augmentations
    fig, axes = plt.subplots(nrows=4, ncols=4)
    for i, ax in enumerate(axes.flat):
        ax.axis('off')
        k = list(sizes.keys())[i // 4]
        im = ax.imshow(sizes[k][i % 4], interpolation='nearest', vmin=-1, vmax=1)

    fig.colorbar(im, ax=axes.ravel().tolist())
    plt.show()


if __name__ == '__main__':
    main()
