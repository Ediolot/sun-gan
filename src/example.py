import time
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from matplotlib.colors import LinearSegmentedColormap

from src.networks.gan.trainer import init
from src.networks.gan.use_networks import load_networks
from src.preprocessing.utils import normalize
from src.tiler.tile_map import TileMap

OUTPUT_IMAGE = Path('../data/generated/{:d}.png'.format(int(time.time())))
SAMPLES = Path('../data/generated_aux/images10000_1579853342_20.npy')  # Pre-generated tiles (see src.networks.gan.use_networks.py)
VECTORS = Path('../data/generated_aux/images10000_1579853342_20_vectors.npy')  # Input vector for the pre-generated tiles
NETWORK = Path('../data/models/1579853342/model/model_20.tar')  # Trained network model
SEED = 998  # Seed for the generated image, use time.time() for random
IMAGE = {
    'size': 10,  # Number of side tiles per image (total tiles = size squared)
    'overlap': 10,  # Overlapping between two tiles in pixels
    'colormap': 'sun',  # set to 'none', 'sun', or any other matplotlib model
}
# Note that output size would be [128*size - overlap*(size-1)] pixels squared

# Custom 'sun' colormap
colors = [(0/255, 0, 0), (156/255, 79/255, 60/255), (223/255, 149/255, 26/255), (230/255, 220/255, 80/255), (1, 1, 1)]
sun_cm = LinearSegmentedColormap.from_list('sun', colors, N=100)


def main():
    device = init(use_cpu=False, seed=SEED)
    samples = np.load(SAMPLES)
    vectors = np.load(VECTORS)  # These are only used in the GA tiler
    net_g, _ = load_networks(NETWORK, device)

    # Generate a complete image with the pre-generated tiles
    # The generated image is random based
    tm, _ = TileMap(IMAGE['size'], IMAGE['size'], 128, IMAGE['overlap']).fill_best(samples)
    img = tm.data

    # Apply the colormap
    if IMAGE['colormap'] != 'none':
        cm = plt.get_cmap(IMAGE['colormap']) if IMAGE['colormap'] != 'sun' else sun_cm
        img = normalize(img, output_range=(0, 1))
        img = cm(img)

    # Save image
    img = normalize(img, output_range=(0, 255))
    img = np.clip(img, a_min=0.0, a_max=255.0).astype(np.uint8)
    Image.fromarray(img).save(OUTPUT_IMAGE)


if __name__ == '__main__':
    main()
