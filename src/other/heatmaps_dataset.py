import matplotlib.pyplot as plt
import numpy as np

from src.common import AUGMENTED_PATH
from src.preprocessing.utils import normalize


# Careful, this needs >16GB of RAM recommended !!


def plot_heatmap(data):
    """ Plot heatmaps for augmented and not augmented images """
    fig, axes = plt.subplots(nrows=1, ncols=2)

    axes.flat[0].imshow(np.average(data[::10, :, :], axis=0), vmin=-1, vmax=1)  # Augmentation has a x10 factor, each 10
    axes.flat[0].set_title('Heatmap (no augmentation)')                         # images a copy of the original is met

    im = axes.flat[1].imshow(np.average(data, axis=0), vmin=-1, vmax=1)
    axes.flat[1].set_title('Heatmap (with augmentation)')

    fig.colorbar(im, ax=axes.ravel().tolist(), shrink=0.65)
    plt.show()


def main():
    file = AUGMENTED_PATH / 'solar_images_augmented.npy'
    print('Loading ' + str(file), end='...')
    data = normalize(np.load(file))
    print('Done!')
    size = data.shape[1]

    # Original size should be 576
    if size == 576:
        data = normalize(data[:, 32:544, 32:544])
        plot_heatmap(data)


if __name__ == '__main__':
    main()
