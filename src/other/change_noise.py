import cv2
import numpy as np
import torch
from tqdm import tqdm

from src.common import MODELS_PATH, NOISE_TEST_VIDEO_PATH
from src.networks.gan.trainer import init
from src.networks.gan.use_networks import load_networks
from src.preprocessing.utils import normalize

NZ = 128              # Size of z latent vector (i.e. size of generator input)
PAUSE = 10            # Seconds the plots will remain on screen
MODEL = '1579853342'  # Model used to generate the heatmaps
EPOCH = 20            # Epoch where the model was saved

VIDEO_FORMAT = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
VIDEO_FPS = 60
VIDEO_SIZE = (128, 128)
CHANGES = 100
STEPS = 100


def noise_test(net_g, device):
    current = torch.randn(1, NZ, 1, 1, device=device)
    writer = cv2.VideoWriter(str(NOISE_TEST_VIDEO_PATH), VIDEO_FORMAT, VIDEO_FPS, VIDEO_SIZE, True)

    for _ in tqdm(range(CHANGES)):
        next = torch.randn(1, NZ, 1, 1, device=device)
        step = (next - current) / STEPS

        for n in range(STEPS):
            current += step

            with torch.no_grad():
                fakes = net_g(current).detach().cpu()

            image = np.transpose(fakes, (0, 2, 3, 1))[0].squeeze()
            image = np.array(image)
            image = normalize(image)
            image = (np.array(image) * 127 + 127).astype('uint8')
            image = np.clip(image, a_max=255, a_min=0)
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
            writer.write(image)

        torch.cuda.empty_cache()
    writer.release()


def main():
    device = init(use_cpu=False)
    path = MODELS_PATH / ('{:s}/model/model_{:d}.tar'.format(MODEL, EPOCH))
    net_g, _ = load_networks(path, device)
    noise_test(net_g, device)


if __name__ == '__main__':
    main()
