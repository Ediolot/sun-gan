import matplotlib.pyplot as plt
import numpy as np

from src.common import MODELS_PATH
from src.networks.gan.trainer import init
from src.networks.gan.use_networks import load_networks, generate_images

NZ = 128              # Size of z latent vector (i.e. size of generator input)
PAUSE = 10            # Seconds the plots will remain on screen
MODEL = '1579853342'  # Model used to generate the heatmaps
EPOCHS = 60           # Epochs where the model was saved


def main():
    device = init(use_cpu=False)

    plt.figure()
    plt.ion()
    plt.show()
    for i in range(EPOCHS // 2):
        path = MODELS_PATH / (MODEL + '/model/model_{:d}.tar'.format(i * 2))
        net_g, net_d = load_networks(path, device)
        images = generate_images(net_g, device, n_images=100, nz=NZ, chunk=10)
        heatmap = np.average(np.array(images), axis=0)

        plt.subplot(6, 5, i+1)
        plt.axis('off')
        plt.gca().set_title('Epoch {:d}'.format(i * 2 + 1))
        plt.imshow(heatmap, vmin=-1, vmax=1)
        plt.draw()
        plt.pause(0.01)

    plt.pause(PAUSE)


if __name__ == '__main__':
    main()
