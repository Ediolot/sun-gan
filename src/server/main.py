import sys

import numpy as np
import json
import png
from colorsys import hsv_to_rgb
from flask import Flask, jsonify, request, abort
from matplotlib.colors import LinearSegmentedColormap

from src.common import GENERATED_IMG_BUFFER
from src.networks.gan.trainer import init
from src.networks.gan.use_networks import load_networks, generate_images
from src.preprocessing.utils import normalize
from src.tiler.tile_map import TileMap
import matplotlib.pyplot as plt
from PIL import Image

app = Flask(__name__)
samples = None
vectors = None
net_g = None
device = None
tm = None

colors = [(0/255, 0, 0), (156/255, 79/255, 60/255), (223/255, 149/255, 26/255), (230/255, 220/255, 80/255), (1, 1, 1)]
sun_cm = LinearSegmentedColormap.from_list('sun', colors, N=100)


@app.route("/load_samples", methods=['POST'])
def load_samples():
    global samples, vectors
    path = request.data.decode("utf-8")
    path = path.replace('//', '/')
    path = path.replace('\\\\', '\\')
    path = path.replace('"', '')

    if '.npy' not in path:
        abort(404)

    try:
        samples = np.load(path)
        vectors = np.load(path.replace('.npy', '_vectors.npy'))
    except Exception as e:
        print(e)
        abort(404)
    return jsonify({
        'data': len(samples)
    })


@app.route("/load_model", methods=['POST'])
def load_model():
    global net_g, device
    path = request.data.decode("utf-8")
    path = path.replace('//', '/')
    path = path.replace('\\\\', '\\')
    path = path.replace('"', '')

    if '.tar' not in path:
        abort(404)

    try:
        net_g, _ = load_networks(path, device)
    except Exception as e:
        print(e)
        abort(404)
    return jsonify({
        'data': 'model'
    })


@app.route("/gen_samples", methods=['POST'])
def gen_samples():
    global samples, vectors, device
    n_samples = int(request.data.decode("utf-8"))

    try:
        samples, vectors = generate_images(net_g, device, n_samples, nz=128, chunk=10)
    except Exception as e:
        print(e)
        abort(404)
    return jsonify({
        'data': len(samples)
    })


@app.route("/gen_image", methods=['POST'])
def gen_image():
    global tm, device, samples
    data = json.loads(request.data)

    try:
        size = int(data[0])
        overlap = int(data[1])
        colormap = data[2]
        tm, _ = TileMap(size, size, 128, overlap).fill_best(samples)
        img = tm.data

        if colormap != 'none':
            cm = plt.get_cmap(colormap) if colormap != 'sun' else sun_cm
            img = normalize(img, output_range=(0, 1))
            img = cm(img)

        normalized = normalize(img, output_range=(0, 255))
        normalized = np.clip(normalized, a_min=0.0, a_max=255.0).astype(np.uint8)
        im = Image.fromarray(normalized)
        im.save(str(GENERATED_IMG_BUFFER))
    except Exception as e:
        print(e)
        abort(404)
    return jsonify({})


@app.route("/recolor", methods=['POST'])
def recolor():
    global tm, device
    colormap = request.data.decode("utf-8")
    colormap = colormap.replace('"', '')

    if tm is None:
        return jsonify({})

    try:
        img = tm.data
        if colormap != 'none':
            cm = plt.get_cmap(colormap) if colormap != 'sun' else sun_cm
            img = normalize(img, output_range=(0, 1))
            img = cm(img)

        normalized = normalize(img, output_range=(0, 255))
        normalized = np.clip(normalized, a_min=0.0, a_max=255.0).astype(np.uint8)
        im = Image.fromarray(normalized)
        im.save(str(GENERATED_IMG_BUFFER))
    except Exception as e:
        print(e)
        abort(404)
    return jsonify({})


if __name__ == "__main__":
    port = 5050 if len(sys.argv) < 2 else int(sys.argv[1])
    device = init(use_cpu=False)
    app.run(port=port)

