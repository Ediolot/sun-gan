from pathlib import Path

HERE = Path(__file__).parent.absolute()
FITS_IN_PATH = (HERE / '../data/simulations/imsol.fits').resolve()
FITS_OUT_PATH = (HERE / '../doc/imsol.png').resolve()
SIMULATIONS_PATH = (HERE / '../data/simulations').resolve()
AUGMENTED_PATH = (HERE / '../data/images').resolve()
TARGET_IMAGES_PATH = (HERE / '../data/images/solar_images_augmented_downsampled_128x128').resolve()
MODELS_PATH = (HERE / '../data/models').resolve()
NOISE_TEST_VIDEO_PATH = (HERE / '../doc/latent_changes.avi').resolve()
GENERATED_IMAGES_AUX = (HERE / '../data/generated_aux').resolve()
GENERATED_IMG_BUFFER = (HERE / '../data/generated/buffer.png').resolve()

ABCD_MINUS = 'abcdefghijklmnopqrstuvwxyz'
ABCD_MAYUS = 'AVCDEFGHIJKLMNOPQRSTUVWXYZ'
