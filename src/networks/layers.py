import copy

import torch
from torch import nn
from torch.nn.init import calculate_gain
from torch.nn.init import kaiming_normal_ as kaiming_normal


class Print(nn.Module):
    """ Print the shape of the data at that layer if debug is True """
    def __init__(self, debug=True, name=""):
        super(Print, self).__init__()
        self.debug = debug
        self.name = name

    def forward(self, x):
        if self.debug:
            print(self.name, x.shape)
        return x


class PixelWiseNorm(nn.Module):
    def __init__(self):
        super(PixelWiseNorm, self).__init__()
        self.eps = 1e-8

    def forward(self, x):
        return x / (torch.mean(x**2, dim=1, keepdim=True) + self.eps) ** 0.5


class EqualizedConv(nn.Module):
    def __init__(self, c_in, c_out, k_size, stride, pad, initializer='kaiming', bias=False):
        super(EqualizedConv, self).__init__()
        self.conv = nn.Conv2d(c_in, c_out, k_size, stride, pad, bias=False)
        kaiming_normal(self.conv.weight, a=calculate_gain('conv2d'))

        conv_w = self.conv.weight.data.clone()
        self.bias = torch.nn.Parameter(torch.FloatTensor(c_out).fill_(0))
        self.scale = (torch.mean(self.conv.weight.data ** 2)) ** 0.5
        self.conv.weight.data.copy_(self.conv.weight.data / self.scale)

    def forward(self, x):
        x = self.conv(x.mul(self.scale))
        return x + self.bias.view(1, -1, 1, 1).expand_as(x)


class EQLinear(nn.Module):
    def __init__(self, c_in, c_out):
        super(EQLinear, self).__init__()
        self.linear = nn.Linear(c_in, c_out, bias=False)
        kaiming_normal(self.linear.weight, a=calculate_gain('linear'))

        self.bias = torch.nn.Parameter(torch.FloatTensor(c_out).fill_(0))
        self.scale = (torch.mean(self.linear.weight.data ** 2)) ** 0.5
        self.linear.weight.data.copy_(self.linear.weight.data / self.scale)

    def forward(self, x):
        x = self.linear(x.mul(self.scale))
        return x + self.bias.view(1, -1).expand_as(x)


class minibatch_std_concat_layer(nn.Module):
    """ Minibatch layer (https://github.com/github-pengge/PyTorch-progressive_growing_of_gans/blob/master/models/base_model.py) """
    def __init__(self):
        super(minibatch_std_concat_layer, self).__init__()
        self.adjusted_std = lambda x, **kwargs: torch.sqrt(torch.mean((x - torch.mean(x, **kwargs)) ** 2, **kwargs) + 1e-8)

    def forward(self, x):
        shape = list(x.size())
        target_shape = copy.deepcopy(shape)
        vals = self.adjusted_std(x, dim=0, keepdim=True)
        target_shape[1] = 1
        vals = torch.mean(vals, dim=1, keepdim=True)
        vals = vals.expand(*target_shape)
        return torch.cat([x, vals], 1)


class ProjectAndReshape(nn.Module):
    """ Project and reshape the input into any output size using a linear layer """
    def __init__(self, nz, out_c, out_w, out_h):
        super(ProjectAndReshape, self).__init__()
        self.nz = nz
        self.out_w = out_w
        self.out_h = out_h
        self.out_c = out_c
        self.linear = nn.Linear(nz, out_w * out_h * out_c)

    def forward(self, x):
        x = x.view(-1, self.nz)
        x = self.linear(x)
        x = x.view(-1, self.out_c, self.out_w, self.out_h)
        return x


class Flatten(nn.Module):
    def __init__(self):
        super(Flatten, self).__init__()

    def forward(self, x):
        return x.view(x.size(0), -1)


class UnFlatten(nn.Module):
    def __init__(self, size):
        super(UnFlatten, self).__init__()
        self.size = size

    def forward(self, x):
        return x.view(x.size(0), self.size, 1, 1)
