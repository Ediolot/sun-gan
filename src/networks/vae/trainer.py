from random import randint

import matplotlib.pyplot as plt
import torch
import torch.nn.functional as F

from src.common import TARGET_IMAGES_PATH
from src.networks.vae.vae import VAE
from src.preprocessing.utils import load_dataset

BATCH_SIZE = 16
EPOCHS = 50
PLOTS = True


def loss_fn(recon_x, x, mu, logvar):
    """ Loss function: BCE + KLD for the VAE """
    BCE = F.mse_loss(recon_x, x, size_average=False)
    KLD = -0.5 * torch.mean(1 + logvar - mu.pow(2) - logvar.exp())
    return BCE + KLD, BCE, KLD


def gen_training_interactive_plot():
    fig = plt.figure()
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132)
    ax3 = fig.add_subplot(133)
    plt.ion()
    plt.show()

    def update_images(fixed_x, recon_x, recon_sx):
        ax1.imshow(fixed_x.detach().cpu().squeeze())
        ax2.imshow(recon_x.detach().cpu().squeeze())
        ax3.imshow(recon_sx.detach().cpu().squeeze())
        plt.draw()
        plt.pause(0.01)

    return update_images


def main():
    dataset, data_loader = load_dataset(path=TARGET_IMAGES_PATH, batch_size=BATCH_SIZE, return_dataset=True)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # VAE model
    vae = VAE().to_(device)
    optimizer = torch.optim.Adam(vae.parameters(), lr=1e-3)

    # Fixed input for debugging
    fixed_x, _ = next(iter(data_loader))

    if PLOTS:
        update_images = gen_training_interactive_plot()

    for epoch in range(EPOCHS):
        for idx, (images, _) in enumerate(data_loader):
            images = images.to(device)
            recon_images, mu, logvar = vae(images)
            loss, bce, kld = loss_fn(recon_images, images, mu, logvar)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            print("Epoch[{}/{}] Loss: {:.3f} {:.3f} {:.3f}".format(epoch + 1, EPOCHS,
                                                                   loss.item() / BATCH_SIZE,
                                                                   bce.item() / BATCH_SIZE,
                                                                   kld.item() / BATCH_SIZE))

        # Plot progress each epoch
        if PLOTS:
            with torch.no_grad():
                sample_x = torch.randn(1, 512).to(device)
                fixed_x = dataset[randint(1, 100)][0].unsqueeze(0).to(device)
                recon_x, _, _ = vae(fixed_x)
                recon_sx = vae.decoder(sample_x)
            update_images(fixed_x, recon_x, recon_sx)


if __name__ == "__main__":
    main()
