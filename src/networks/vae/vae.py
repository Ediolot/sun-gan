import torch.nn as nn
import torch

from src.networks.layers import EqualizedConv, PixelWiseNorm, Print, Flatten, UnFlatten


class VAE(nn.Module):
    """ VAE used in the report. """
    def __init__(self, h_dim=512, z_dim=256):
        super(VAE, self).__init__()
        self.device = 'cpu'  # Default device
        self.encoder = nn.Sequential(
            # 1x128x128
            EqualizedConv(1, 256, k_size=1, stride=1, pad=0), nn.LeakyReLU(0.2, inplace=True),
            # 256x128x128
            nn.Upsample(scale_factor=0.5, mode='bilinear'), # 256 64x64
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(256),
            # 256x64x64
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(256),
            # 256x32x32
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(256),
            # 256x16x16
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(256),
            # 256x8x8
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(256),
            # 256x4x4
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(256, 128, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(128),
            # 258x2x2
            Flatten(),
            # 512
        )

        self.fc1 = nn.Linear(h_dim, z_dim)
        self.fc2 = nn.Linear(h_dim, z_dim)
        self.fc3 = nn.Linear(z_dim, h_dim)

        self.decoder = nn.Sequential(
            # 512
            UnFlatten(size=512),
            # 512x1x1
            EqualizedConv(512, 256, k_size=4, stride=1, pad=3), nn.LeakyReLU(0.2, inplace=True), PixelWiseNorm(),
            # 256x4x4
            nn.Upsample(scale_factor=2, mode='nearest'), # 256 8 8
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), PixelWiseNorm(),
            # 256x8x8
            nn.Upsample(scale_factor=2, mode='nearest'), # 256 16 16
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), PixelWiseNorm(),
            # 256x16x16
            nn.Upsample(scale_factor=2, mode='nearest'), # 256 32 32
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), PixelWiseNorm(),
            # 256x32x32
            nn.Upsample(scale_factor=2, mode='nearest'), # 256 64 64
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), PixelWiseNorm(),
            # 256x64x64
            nn.Upsample(scale_factor=2, mode='nearest'), # 256 128 128
            EqualizedConv(256, 256, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), PixelWiseNorm(),
            # 256x128x128
            EqualizedConv(256, 1, k_size=1, stride=1, pad=0),
            # 1x128x128
        )

    def to_(self, device):
        self.device = device
        return self.to(device)

    def reparameterize(self, mu, logvar):
        std = logvar.mul(0.5).exp_()
        # return torch.normal(mu, std)
        esp = torch.randn(*mu.size()).to(self.device)
        z = mu + std * esp
        return z

    def bottleneck(self, h):
        mu, logvar = self.fc1(h), self.fc2(h)
        z = self.reparameterize(mu, logvar)
        return z, mu, logvar

    def encode(self, x):
        h = self.encoder(x)
        z, mu, logvar = self.bottleneck(h)
        return z, mu, logvar

    def decode(self, z):
        z = self.fc3(z)
        z = self.decoder(z)
        return z

    def forward(self, x):
        z, mu, logvar = self.encode(x)
        z = self.decode(z)
        return z, mu, logvar
