import torch.nn as nn


class SRCNN(nn.Module):
    """ Super resolution CNN """
    def __init__(self):
        super(SRCNN, self).__init__()

        self.blocks = nn.Sequential(
            nn.Conv2d(1, 32,    kernel_size=3, stride=1, padding=1), nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(32, 32,   kernel_size=3, stride=1, padding=1), nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(32, 64,   kernel_size=3, stride=1, padding=1), nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(64, 64,   kernel_size=3, stride=1, padding=1), nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(64, 128,  kernel_size=3, stride=1, padding=1), nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1), nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(128, 1,   kernel_size=3, stride=1, padding=1),
        )

    def forward(self, x):
        return self.blocks(x)

    def weights_init(self):
        self.apply(SRCNN.__weights_init)

    @staticmethod
    def __weights_init(m):
        classname = m.__class__.__name__
        if classname.find('Conv') != -1:
            nn.init.normal_(m.weight.data, 0.0, 0.02)
        elif classname.find('BatchNorm') != -1:
            nn.init.normal_(m.weight.data, 1.0, 0.02)
            nn.init.constant_(m.bias.data, 0)
