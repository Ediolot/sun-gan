import json
import random
import time
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torchvision.utils import make_grid

from src.networks.gan.discriminator import Discriminator
from src.networks.gan.generator import Generator
from src.preprocessing.utils import pil_grayscale_saver, load_dataset
from src.common import TARGET_IMAGES_PATH, MODELS_PATH


timestamp = str(int(time.time()))                        # Gives a nme to the model to store
OUTPUT_DIR_PATH = (MODELS_PATH / (timestamp + '/'))      # Output directory for this model
PLOTS = True
USE_CPU = False
GPU_NAME = 'cuda:0'

SEED = 999
REAL_LABEL = 1
FAKE_LABEL = 0
BATCH_SIZE = 8
ITER_DISPLAY = 2200 / 20  # Number of images processed to wait (aprox.) before updating the plot
NZ = 128                  # Size of z latent vector (i.e. size of generator input)
LR = 0.001                # Learning rate for optimizers
BETA1 = 0.0               # Beta1 hyperparam for Adam optimizers
BETA2 = 0.99              # Beta2 hyperparam for Adam optimizers
N_EPOCHS = 200            # Number of training epochs. Model is saved each epoch !!
TS_SIZE = 15              # Maximum number of time values to store to approximate ETA for training


def init(use_cpu=False, cuda_name='cuda:0', seed=SEED):
    random.seed(seed)
    torch.manual_seed(seed)

    if use_cpu or not torch.cuda.is_available():
        device_name = 'cpu'
    else:
        device_name = cuda_name

    device = torch.device(device_name)
    print('Using device', device, 'Random seed:', seed)
    return device


def update_discriminator(net_d, fakes, optimizer_d, batch, criterion, labels):
    """ Update D network: maximize log(D(x)) + log(1 - D(G(z))) """
    net_d.zero_grad()  # PyTorch accumulates the gradients, so clear them

    # Forward with real
    output = net_d(batch).view(-1)  # .view(-1) to flatten the output
    # Train with real, errors
    error_real = criterion(output, labels)
    error_real.backward()  # Calculate gradients for D in backward pass
    d_x = output.mean().item()

    # Forward with fake
    output = net_d(fakes.detach()).view(-1)  # .view(-1) to flatten the output
    # Train with fake, errors
    labels.fill_(FAKE_LABEL)
    error_fake = criterion(output, labels)
    error_fake.backward()
    d_g_z1 = output.mean().item()

    # Done
    optimizer_d.step()
    return d_x, d_g_z1, error_real + error_fake


def update_generator(net_d, net_g, optimizer_g, criterion, fakes, labels):
    """ Update G network: maximize log(D(G(z))) """
    net_g.zero_grad()
    labels.fill_(REAL_LABEL)

    # Forward discriminator with fake again, since it was just updated
    output = net_d(fakes).view(-1)
    # Train generator
    error = criterion(output, labels)
    error.backward()
    d_g_z2 = output.mean().item()
    
    # Done
    optimizer_g.step()
    return error, d_g_z2


def gen_training_interactive_plot():
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    plt.ion()
    plt.show()

    def update_progression(g_losses, d_losses):
        ax1.clear()
        ax1.plot(g_losses, label="G")
        ax1.plot(d_losses, label="D")
        ax1.legend()
        ax1.set_xlabel("iterations")
        ax1.set_ylabel("Loss")
        ax1.set_yscale('log')
        plt.draw()
        plt.pause(0.01)

    def update_images(grid):
        ax2.clear()
        ax2.axis("off")
        ax2.imshow(grid, vmin=-1, vmax=1)
        plt.draw()
        plt.pause(0.01)

    return update_progression, update_images


def plot_real_vs_fake(data_loader, device, img_list):
    batch = next(iter(data_loader))
    grid_real = make_grid(batch[0].to(device)[:64], padding=5, normalize=True).cpu()
    grid_real = np.transpose(grid_real, (1, 2, 0))

    plt.ioff()
    plt.subplot(121)
    plt.axis("off")
    plt.title("Real Images")
    plt.imshow(grid_real)
    plt.subplot(122)
    plt.axis("off")
    plt.title("Fake Images")
    plt.imshow(img_list[-1])
    plt.show()


def save_model(net_g, net_d, optimizer_g, optimizer_d, epoch, output_dir: Path):
    model_dir_path = output_dir / 'model/'
    model_path = output_dir / ('model/model_' + str(epoch) + '.tar')
    model_dir_path.mkdir(parents=True, exist_ok=True)
    torch.save({
        'epoch': epoch,
        'modelG_state_dict': net_g.state_dict(),
        'optimizer_g_state_dict': optimizer_g.state_dict(),
        'modelD_state_dict': net_d.state_dict(),
        'optimizer_d_state_dict': optimizer_d.state_dict()
    }, model_path)
    print('Saved!', model_path)


def train(net_g, net_d, optimizer_g, optimizer_d, criterion, data_loader, device):
    img_list = []   # Sample of generated images each time iter == ITER_DISPLAY
    img_names = []  # Name used to store images each time iter == ITER_DISPLAY
    g_losses = []   # Generator losses
    d_losses = []   # Discriminator losses
    iters = 0       # Number of images processed 
    last_ts = []    # Store time take by last train batch to approximate ETA
    fixed_noise = torch.randn(8, NZ, 1, 1, device=device)  # 8 images to save a visual of generator progression

    if PLOTS:
        update_progression, update_images = gen_training_interactive_plot()

    for epoch in range(N_EPOCHS):
        for i, batch in enumerate(data_loader):  # Foreach batch
            t0 = time.time()

            # Batch of real images
            print(batch)
            batch_len = batch[0].size(0)
            batch_dev = batch[0].to(device)
            # Batch of fake images
            noise = torch.randn(batch_len, NZ, 1, 1, device=device)
            fakes = net_g(noise)  # Generate fake images batch from latent vectors

            # Real and fake images labels
            labels = torch.full((batch_len,), REAL_LABEL, device=device)

            # Train networks
            d_x, d_g_z1, err_d = update_discriminator(net_d, fakes, optimizer_d, batch_dev, criterion, labels)
            err_g, d_g_z2 = update_generator(net_d, net_g, optimizer_g, criterion, fakes, labels)

            # Store time taken
            t1 = time.time()
            last_ts.append(t1 - t0)
            if len(last_ts) > TS_SIZE:
                last_ts.pop(0)

            # Output training stats
            eta = ((N_EPOCHS - epoch) * len(data_loader) - i) * np.average(last_ts) / 60.0
            print('EPOCH[{:d}/{:d}] BATCH[{:d}/{:d}]'.format(epoch, N_EPOCHS, i, len(data_loader)), end='\t')
            print('Loss_D: {:.4f}\tLoss_G: {:.4f}\tD(x): {:.4f}\tD(G(z)): {:.4f} / {:.4f}'.format(
                  err_d.item(), err_g.item(), d_x, d_g_z1, d_g_z2), end='\t')
            print('Took {:.3} ETA: {:.1f} min'.format(t1 - t0, eta))

            # Save Losses for plotting later
            g_losses.append(err_g.item())
            d_losses.append(err_d.item())

            # Check how the generator is doing by saving G's output on fixed_noise
            # and update plots
            iters += batch_len
            if (iters > ITER_DISPLAY) or ((epoch == N_EPOCHS - 1) and (i == len(data_loader) - 1)):
                iters = 0

                with torch.no_grad():
                    fakes = net_g(fixed_noise).detach().cpu()

                grid = make_grid(fakes, padding=2, normalize=True, nrow=1)  # All in the same line
                grid = np.transpose(grid, (1, 2, 0))
                img_list.append(grid)
                img_names.append('epoch%d_batch%d' % (epoch, i))

                if PLOTS:
                    stacked = torch.cat([fakes[0], fakes[1], batch[0][0]], 1).squeeze()
                    update_progression(g_losses, d_losses)
                    update_images(stacked)

            save_model(net_g, net_d, optimizer_g, optimizer_d, epoch, OUTPUT_DIR_PATH)

    # Save loses and images
    with (OUTPUT_DIR_PATH / 'loses.json').open('w') as f:
        json.dump({
            'g_losses': g_losses,
            'd_losses': d_losses
        }, f)
    for i, img in enumerate(img_list):
        pil_grayscale_saver(OUTPUT_DIR_PATH / (img_names[i] + '.png'), img)

    if PLOTS:
        plot_real_vs_fake(data_loader, device, img_list)


def main():
    device = init(use_cpu=USE_CPU, cuda_name=GPU_NAME)
    data_loader = load_dataset(path=TARGET_IMAGES_PATH, batch_size=BATCH_SIZE)

    # Networks
    net_g = Generator().to(device)
    net_d = Discriminator().to(device)
    net_g.weights_init()
    net_d.weights_init()

    # Train
    optimizer_d = optim.Adam(net_d.parameters(), lr=LR, betas=(BETA1, BETA2), weight_decay=0)
    optimizer_g = optim.Adam(net_g.parameters(), lr=LR, betas=(BETA1, BETA2), weight_decay=0)
    criterion = nn.BCELoss()  # Binary Cross Entropy (for discriminator)
    train(net_g, net_d, optimizer_g, optimizer_d, criterion, data_loader, device)


if __name__ == '__main__':
    main()
