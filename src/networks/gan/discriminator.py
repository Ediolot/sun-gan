import torch.nn as nn

from src.networks.layers import EqualizedConv


class Discriminator(nn.Module):
    """ Discriminator used in the the report. Commented out code was used for progressive growing of GANs. """
    def __init__(self):
        super(Discriminator, self).__init__()
        # self.resl = 4
        # self.alpha = 0

        self.model = nn.Sequential()
        # self.down_scale = nn.AvgPool2d(kernel_size=2)
        # self.next_from_rgb = nn.Sequential()
        # self.next_block = nn.Sequential()

        self.from_rgb = nn.Sequential(
            # 128x128
            EqualizedConv(1, 512, k_size=1, stride=1, pad=0),
            nn.LeakyReLU(0.2, inplace=True),
        )
        self.model.add_module("first_block", nn.Sequential(
            # 512x128x128
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(512),
            # 512x64x64
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(512),
            # 512x32x32
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(512),
            # 512x16x16
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(512),
            # 512x8x8
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2, inplace=True), nn.BatchNorm2d(512),
            # 512x4x4
            nn.Upsample(scale_factor=0.5, mode='bilinear'),
            EqualizedConv(512, 1, k_size=4, stride=1, pad=0),
            nn.Sigmoid()
            # 1
        ))

    def forward(self, x):
        x = self.from_rgb(x)
        x = self.model(x)
        return x
        # if self.resl <= 4:
        #     x = self.from_rgb(x)
        #     x = self.model(x)
        #     return x
        # elif self.resl == 5:
        #     next_x = self.next_from_rgb(x)
        #     next_x = self.next_block(next_x)
        #     x = self.down_scale(x)
        #     x = self.from_rgb(x)
        #     join_x = torch.add(x.mul(1.0 - self.alpha), next_x.mul(self.alpha))
        #     return self.model(join_x)

    def weights_init(self):
        self.apply(Discriminator.__weights_init)

    # def grow_network(self, device):
    #     if self.resl > 4:
    #         self.from_rgb = self.next_from_rgb
    #
    #         new_model = nn.Sequential()
    #         new_model.add_module('nx_block', self.next_block)
    #         for name, module in self.model.named_children():
    #             new_model.add_module(name, module)                      # make new structure and,
    #             new_model[-1].load_state_dict(module.state_dict())      # copy pretrained weights
    #         self.model = None
    #         self.model = new_model.to(device)
    #
    #     self.resl += 1
    #     self.next_from_rgb = nn.Sequential(
    #         nn.Conv2d(1, 512, 1, 1, 0, bias=False),
    #         nn.LeakyReLU(0.2, inplace=True),
    #     ).to(device)
    #     self.next_block = nn.Sequential(
    #         nn.Conv2d(512, 512, 4, 2, 1, bias=False),
    #         nn.BatchNorm2d(512),
    #         nn.LeakyReLU(0.2, inplace=True),
    #     ).to(device)
    #
    #     self.next_block.apply(Discriminator.__weights_init)
    #     self.next_from_rgb.apply(Discriminator.__weights_init)

    # def update_alpha(self, delta=0.1):
    #     self.alpha = min(1.0, self.alpha + delta)

    @staticmethod
    def __weights_init(m):
        classname = m.__class__.__name__
        if classname.find('Conv2d') != -1:
            nn.init.normal_(m.weight.data, 0.0, 0.02)
        elif classname.find('BatchNorm') != -1:
            nn.init.normal_(m.weight.data, 1.0, 0.02)
            nn.init.constant_(m.bias.data, 0)
