import torch.nn as nn

from src.networks.layers import EqualizedConv, PixelWiseNorm


class Generator(nn.Module):
    """ Generator used in the the report. Commented out code was used for progressive growing of GANs. """
    def __init__(self):
        super(Generator, self).__init__()
        # self.resl = 4
        # self.alpha = 0

        # self.up_sample = nn.Upsample(scale_factor=2, mode='nearest')
        # self.next_block = nn.Sequential()
        # self.next_to_rgb = nn.Sequential()
        self.model = nn.Sequential()
        self.model.add_module("first_block", nn.Sequential(
            # 512x1x1
            EqualizedConv(128, 512, k_size=4, stride=1, pad=3), nn.LeakyReLU(0.2), PixelWiseNorm(),
            # 512x4x4
            nn.Upsample(scale_factor=2, mode='nearest'),
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2), PixelWiseNorm(),
            # 512x8x8
            nn.Upsample(scale_factor=2, mode='nearest'),
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2), PixelWiseNorm(),
            # 512x16x16
            nn.Upsample(scale_factor=2, mode='nearest'),
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2), PixelWiseNorm(),
            # 512x32x32
            nn.Upsample(scale_factor=2, mode='nearest'),
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2), PixelWiseNorm(),
            # 512x64x64
            nn.Upsample(scale_factor=2, mode='nearest'),
            EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2), PixelWiseNorm(),
            # 512x128x128
        ))
        self.to_rgb = nn.Sequential(
            # 512x128x128
            EqualizedConv(512, 1, k_size=1, stride=1, pad=0),
            # 1x128x128
        )

    def forward(self, x):
        x = self.model(x)
        x = self.to_rgb(x)
        return x
        # if self.resl <= 4:
        #     x = self.model(x)
        #     x = self.to_rgb(x)
        #     return x
        # elif self.resl == 5:
        #     x = self.model(x)  # TODO clone ?
        #     next_x = self.next_block(x)  # Already contains upsample
        #     x = self.up_sample(x)
        #     x = self.to_rgb(x)
        #     next_x = self.next_to_rgb(next_x)
        #     return torch.add(x.mul(1.0 - self.alpha), next_x.mul(self.alpha))

    # def grow_network(self, device):
    #     if self.resl > 4:
    #         self.to_rgb = self.next_to_rgb
    #         self.model.add_module('nx_block', self.next_block)
    #
    #     self.resl += 1
    #     self.next_block = nn.Sequential(
    #         nn.Upsample(scale_factor=2, mode='nearest'),
    #         EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2), PixelWiseNorm(),
    #         # EqualizedConv(512, 512, k_size=3, stride=1, pad=1), nn.LeakyReLU(0.2), PixelWiseNorm(),
    #     ).to(device)
    #     self.next_to_rgb = nn.Sequential(
    #         EqualizedConv(512, 1, k_size=1, stride=1, pad=0),
    #     ).to(device)
    #
    #     self.next_block.apply(Generator.__weights_init)
    #     self.next_to_rgb.apply(Generator.__weights_init)

    # def update_alpha(self, delta=0.1):
    #     self.alpha = min(1.0, self.alpha + delta)

    def weights_init(self):
        self.apply(Generator.__weights_init)

    @staticmethod
    def __weights_init(m):
        classname = m.__class__.__name__
        if classname.find('Conv2d') != -1:
            nn.init.normal_(m.weight.data, 0.0, 0.02)
        elif classname.find('BatchNorm') != -1:
            nn.init.normal_(m.weight.data, 1.0, 0.02)
            nn.init.constant_(m.bias.data, 0)
