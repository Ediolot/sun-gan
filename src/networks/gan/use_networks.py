import numpy as np
import torch
from tqdm import tqdm

from src.common import MODELS_PATH, GENERATED_IMAGES_AUX
from src.networks.gan.discriminator import Discriminator
from src.networks.gan.generator import Generator
from src.networks.gan.trainer import init

MODEL = '1579853342'  # Model used to generate the heatmaps
EPOCH = 20            # Epoch where the model was saved
N_IMAGES = 30_000    # Images to generate
NZ = 128              # Latent vector size


def load_networks(path, device):
    """ Load discriminator and generator """
    net_g = Generator().to(device)
    net_d = Discriminator().to(device)
    net_g.weights_init()
    net_d.weights_init()

    checkpoint = torch.load(path)
    net_g.load_state_dict(checkpoint['modelG_state_dict'])
    net_d.load_state_dict(checkpoint['modelD_state_dict'])
    net_g.eval()
    net_d.eval()
    return net_g, net_d


def generate_images(net_g, device, n_images, nz, chunk=10):
    """ Generate n_images using the generator net_g in chunks. Each n chunk of N chunks (N=n_images/chunk) is one call
     to the GPU. Maximum chunk size depends on the network and the GPU VRAM. """
    images = []
    vectors = []
    chunks = [chunk for _ in range(n_images // chunk)]
    remainder = n_images - chunk * (n_images // chunk)

    if remainder != 0:
        chunks.append(remainder)

    for chunk in tqdm(chunks, desc='Generating images'):
        noise = torch.randn(chunk, nz, 1, 1, device=device)
        with torch.no_grad():
            fakes = net_g(noise).detach().cpu()
            torch.cuda.empty_cache()
        transposed = np.array(np.transpose(fakes, (0, 2, 3, 1)))
        images.extend([image.squeeze() for image in list(transposed)])
        vectors.extend([vector.reshape(nz, 1, 1) for vector in list(noise.detach().cpu())])

    return images, vectors


def generate_single_image(net_g, device, z_vector):
    """ Execute the generator over a single z_vector """
    with torch.no_grad():
        gene = torch.from_numpy(z_vector).float().to(device)
        fakes = net_g(gene).detach().cpu()
        torch.cuda.empty_cache()
    return np.array(np.transpose(fakes, (0, 2, 3, 1)))[0, :, :, :].squeeze()


if __name__ == '__main__':
    device = init(use_cpu=False)
    path = MODELS_PATH / ('{:s}/model/model_{:d}.tar'.format(MODEL, EPOCH))
    net_g, _ = load_networks(path, device)
    images, vectors = generate_images(net_g, device, N_IMAGES, nz=NZ)

    save_path = GENERATED_IMAGES_AUX / 'images{:d}_{:s}_{:d}.npy'.format(N_IMAGES, MODEL, EPOCH)
    np.save(str(save_path), np.array([np.array(i) for i in images]))

    save_path = GENERATED_IMAGES_AUX / 'images{:d}_{:s}_{:d}_vectors.npy'.format(N_IMAGES, MODEL, EPOCH)
    np.save(str(save_path), np.array([np.array(i) for i in vectors]))
