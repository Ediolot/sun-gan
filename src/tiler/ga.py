import numpy as np
import torch


class GA:
    """ GA search for the generator network """
    def __init__(self, net_g, tm, device, pop_size=60, gen_size=128):
        self.device = device
        self.net_g = net_g
        self.tm = tm
        self.pop = []
        self.pop_size = pop_size
        self.gen_size = gen_size

        chunk_size = 10
        self.chunks = [chunk_size for _ in range(self.pop_size // chunk_size)]
        remainder = self.pop_size - chunk_size * (self.pop_size // chunk_size)
        if remainder != 0:
            self.chunks.append(remainder)

    def calc_fitness(self, left):
        fitness = []
        start = 0
        for chunk in self.chunks:
            noise = torch.from_numpy(self.pop[start:start+chunk]).float().to(self.device)
            with torch.no_grad():
                fakes = self.net_g(noise).detach().cpu()
            images = np.array(np.transpose(fakes, (0, 2, 3, 1))).squeeze()
            fitness.extend([self.tm.right_distance(left, image) for image in images])
            start += chunk

        mi = np.min(fitness)
        fitness = np.max(fitness) - fitness
        return fitness / np.sum(fitness), mi

    def find(self, left, base=None, max_iter=500, threshold=0.40):
        self.pop = np.random.uniform(-1, 1, (self.pop_size, self.gen_size, 1, 1))
        best_gene = None
        best_distance = None

        if base is not None:
            for i in range(20):
                self.pop[i, :, :, :] = np.copy(base)
            self.pop[:19, :, :, :] += np.random.uniform(-0.1, 0.1, (19, self.gen_size, 1, 1))

        print()
        for i in range(max_iter):
            new_pop = np.empty((self.pop_size, self.gen_size, 1, 1))

            # Calculate fitness values
            fitness, mi = self.calc_fitness(left)

            print('\rGA Best distance: {:.5f} [Threshold: {:.5f}] iter: {:d}/{:d}'.format(mi, threshold, i, max_iter), end='')
            if best_distance is None or mi < best_distance:
                best_distance = mi
                best_gene = np.copy(self.pop[np.argmax(fitness)].reshape((1, self.gen_size, 1, 1)))
            if mi < threshold:
                break

            # Select top 10 individuals to remain in the population
            fitness_idx = np.argsort(fitness)  # Ascending order
            new_pop[:10, :, :, :] = np.copy(self.pop[fitness_idx[-10:], :, :, :])

            # Generate 80 new individuals from crossover
            # Select parents with roulette selection
            cumsum = np.cumsum(fitness[fitness_idx])

            for j in range(60 - 10 - 10):
                idx0 = next(i for i, v in enumerate(cumsum) if v >= np.random.uniform(0, 1))
                idx1 = idx0
                while idx1 == idx0:
                    idx1 = next(i for i, v in enumerate(cumsum) if v >= np.random.uniform(0, 1))

                crossover = np.random.randint(self.gen_size)
                new_pop[10 + j, :crossover, :, :] = np.copy(self.pop[idx0, :crossover, :, :])
                new_pop[10 + j, crossover:, :, :] = np.copy(self.pop[idx1, crossover:, :, :])

            # Generate 10 new individuals randomly
            new_pop[-10:, :, :, :] = np.random.uniform(-1, 1, (10, self.gen_size, 1, 1))

            # Mutation
            new_pop[:, :, :, :] += np.random.uniform(-0.1, 0.1, (self.pop_size, self.gen_size, 1, 1))
            self.pop = new_pop

        print()
        return best_gene, best_distance
