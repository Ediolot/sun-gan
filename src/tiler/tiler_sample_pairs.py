import matplotlib.pyplot as plt
import numpy as np

from src.common import GENERATED_IMAGES_AUX, MODELS_PATH
from src.networks.gan.trainer import init
from src.networks.gan.use_networks import load_networks, generate_single_image
from src.tiler.ga import GA
from src.tiler.tile_map import TileMap

MODEL = '1579853342'  # Model used
EPOCH = 20            # Epoch where the model was saved
NZ = 128              # Size of z latent vector (i.e. size of generator input)
GENERATOR_IMAGES_PATH = GENERATED_IMAGES_AUX / 'images10000_{:s}_{:d}.npy'.format(MODEL, EPOCH)
GENERATOR_VECTORS_PATH = GENERATED_IMAGES_AUX / 'images10000_{:s}_{:d}_vectors.npy'.format(MODEL, EPOCH)
MODEL_PATH = MODELS_PATH / ('{:s}/model/model_{:d}.tar'.format(MODEL, EPOCH))
LEFT_INDEX = 0
OVERLAP = 10


def main():
    device = init()
    net_g, net_d = load_networks(MODEL_PATH, device)
    images = np.load(GENERATOR_IMAGES_PATH)
    vectors = np.load(GENERATOR_VECTORS_PATH)

    # This is just for demonstration, do not use these methods directly !!

    # Normal blending
    tm = TileMap(1, 2, 128, OVERLAP)
    right, distance = tm.best_match(images, set(), left_index=LEFT_INDEX)
    tm.set_tile(0, 0, images, LEFT_INDEX)
    tm.set_tile(1, 0, images, right, blend_left=True)
    # PLOT
    plt.subplot(221)
    plt.gca().set_title('Blending d={:.3f}'.format(distance))
    plt.imshow(tm.data)

    # Overlapping #1
    img = np.empty(shape=(128, 128 * 2 - OVERLAP))
    img[:, :128]      = images[LEFT_INDEX]
    img[:, 128 - OVERLAP:] = images[right]
    # PLOT
    plt.subplot(222)
    plt.gca().set_title('No blending, right overlaps left')
    plt.imshow(img)

    # Overlapping #2
    img = np.empty(shape=(128, 128 * 2 - OVERLAP))
    img[:, :128] = images[LEFT_INDEX]
    img[:, 128:] = images[right, :, :-OVERLAP]
    # PLOT
    plt.subplot(224)
    plt.gca().set_title('No blending, left overlaps rights')
    plt.imshow(img)

    # Blending with GA
    ga = GA(net_g, tm, device)
    gene, distance = ga.find(images[LEFT_INDEX], base=vectors[right])
    image = generate_single_image(net_g, device, gene)

    tm.set_tile(0, 0, images, LEFT_INDEX)
    tm.set_tile(1, 0, [image], blend_left=True, use_index=False)

    # PLOT
    plt.subplot(223)
    plt.gca().set_title('Blending + GA d={:.3f}'.format(distance))
    plt.imshow(tm.data)
    plt.show()


if __name__ == '__main__':
    main()
