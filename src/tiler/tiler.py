import matplotlib.pyplot as plt
import numpy as np
import torch

from src.common import GENERATED_IMAGES_AUX, MODELS_PATH
from src.networks.gan.trainer import init
from src.networks.gan.use_networks import load_networks
from src.tiler.ga import GA
from src.tiler.tile_map import TileMap

MODEL = '1579853342'  # Model used
EPOCH = 20            # Epoch where the model was saved
NZ = 128              # Size of z latent vector (i.e. size of generator input)
GENERATOR_IMAGES_PATH = GENERATED_IMAGES_AUX / 'images10000_{:s}_{:d}.npy'.format(MODEL, EPOCH)
MODEL_PATH = MODELS_PATH / ('{:s}/model/model_{:d}.tar'.format(MODEL, EPOCH))


def fill_basic(images, tile_size, shape, overlap):
    return TileMap(shape[0], shape[1], tile_size, overlap).sequential_fill(images)


def main():
    device = init()
    net_g, net_d = load_networks(MODEL_PATH, device)
    images = np.load(GENERATOR_IMAGES_PATH)

    # # Sequential fill (bad tiling)
    # tile_map = TileMap(4, 4, 128, 10).sequential_fill(images)
    # plt.imshow(tile_map.data)
    # plt.show()
    #
    # # Best fill (greedy search tiling)
    # tile_map = TileMap(4, 4, 128, 10).fill_best(images)
    # plt.imshow(tile_map.data)
    # plt.show()

    left = 6
    overlap = 10
    tm = TileMap(1, 2, 128, overlap)
    right, d = tm.best_match(images, set(), left_index=left)
    tm.set_tile(0, 0, images, left)
    tm.set_tile(1, 0, images, right, blend_left=True)
    print(d)
    plt.subplot(221)
    plt.gca().set_title('Blending')
    plt.imshow(tm.data)
    plt.subplot(222)
    tm1 = np.empty(shape=(128, 128 * 2 - overlap))
    tm1[:, :128] = images[left]
    tm1[:, 128 - 10:] = images[right]
    plt.gca().set_title('No blending, right overlaps left')
    plt.imshow(tm1)
    plt.subplot(224)
    tm2 = np.empty(shape=(128, 128 * 2 - overlap))
    tm2[:, :128] = images[left]
    tm2[:, 128:] = images[right, :, :-10]
    plt.gca().set_title('No blending, left overlaps rights')
    plt.imshow(tm2)
    plt.show()
    exit()

    tm3 = TileMap(1, 2, 128, overlap)
    ga = GA(net_g, tm3, device)
    gene = ga.find(images[left])
    with torch.no_grad():
        gene = torch.from_numpy(gene).float().to(device)
        fakes = net_g(gene).detach().cpu()
        torch.cuda.empty_cache()
    image = np.array(np.transpose(fakes, (0, 2, 3, 1)))[0, :, :, :].squeeze()
    tm3.set_tile(0, 0, images, left)
    tm3.set_image_donotuse(1, 0, image, blend_left=True)
    plt.subplot(223)
    plt.gca().set_title('Blending + GA')
    plt.imshow(tm3.data)
    plt.show()
    plt.show()


if __name__ == '__main__':
    main()
