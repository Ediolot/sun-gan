import random

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from tqdm import tqdm

from src.common import GENERATED_IMAGES_AUX
from src.tiler.tile_map import TileMap

MODEL = '1579853342'  # Model used
EPOCH = 20            # Epoch where the model was saved
GENERATOR_IMAGES_PATH = GENERATED_IMAGES_AUX / 'images10000_{:s}_{:d}.npy'.format(MODEL, EPOCH)


def fit_func(x, a, b):
    return a * np.log(x) + b


def main():
    images = np.load(GENERATOR_IMAGES_PATH)

    colors = ['royalblue', 'crimson', 'yellowgreen', 'teal', 'blueviolet', 'olivedrab', 'red']
    images = list(images)
    size_start = 3
    size_step = 4
    size_steps = 3
    size_end = size_steps * size_step + size_start

    x = np.logspace(np.log10(size_start ** 2), np.log10(len(images)), num=100, dtype='int')

    for i, size in enumerate(range(size_start, size_end, size_step)):
        tm = TileMap(size, size, 128, 10)

        x_ = x[next(i for i, v in enumerate(x) if v >= size ** 2):]
        avg = np.empty(len(x_))
        std = np.empty(len(x_))

        for j, n_samples in enumerate(tqdm(x_, desc='Filling %d ** 2 tiles with different pool sizes' % size)):
            subset = np.array(random.sample(images, n_samples))
            _, diffs = tm.fill_best(subset)
            avg[j] = np.amax(diffs)
            std[j] = np.std(diffs)

        consts, _ = curve_fit(fit_func, x_, avg)
        plt.plot(x_, fit_func(x_, *consts),
                 'r-',
                 color=colors[i],
                 linewidth=3,
                 alpha=0.3
                 )

        plt.errorbar(x_, avg, std,
                     label='Tiles: {:d} | {:.3f}ln(x) {:+.3f}'.format(size ** 2, consts[0], consts[1]),
                     fmt="s",
                     elinewidth=0.1,
                     ecolor='k',
                     capsize=5,
                     capthick=0.1,
                     color=colors[i],
                     markersize=4
                     )

    plt.xlabel('Number of tiles')
    plt.ylabel('Distance of stitches')
    plt.xscale('log')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
