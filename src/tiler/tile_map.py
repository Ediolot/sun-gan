import random

import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm
from tqdm import tqdm


class TileMap:

    def __init__(self, rows, cols, tile_size, overlap):
        self.data = np.zeros(shape=(rows * tile_size - (rows - 1) * overlap, cols * tile_size - (cols - 1) * overlap))
        self.index = np.ones(shape=(rows, cols), dtype='int') * (-1)
        self.rows = rows
        self.cols = cols
        self.overlap = overlap
        self.tile_size = tile_size

        self.right_kernel = np.zeros((self.overlap, self.overlap))
        if overlap > 0:
            x = np.linspace(-2.2, 2.2, self.overlap)
            n = norm.pdf(x, 0, 1)
            n = n / np.max(n)
            np.fill_diagonal(self.right_kernel, n)

    def sequential_fill(self, images):
        assert len(images) > (self.rows * self.cols)
        for i, (x, y) in enumerate(self.iterate()):
            self.set_tile(x, y, images, i)
        return self

    def set_tile(self, x, y, images, index=None, blend_top=False, blend_left=False, use_index=True):
        assert 0 <= x < self.cols and 0 <= y < self.rows
        start_x = x * self.tile_size - self.overlap * x
        start_y = y * self.tile_size - self.overlap * y
        end_x = (x + 1) * self.tile_size - self.overlap * x
        end_y = (y + 1) * self.tile_size - self.overlap * y

        if use_index:
            self.index[y, x] = index
            image = images[index]
        else:
            image = images[0]

        if not blend_top and not blend_left:
            self.data[start_y:end_y, start_x:end_x] = image

        if blend_left and not blend_top:
            for i, alpha in enumerate(list(np.linspace(0, 1, self.overlap))):
                self.data[start_y:end_y, start_x + i] = image[:, i] * alpha + self.data[start_y:end_y, start_x + i] * (1 - alpha)
            self.data[start_y:end_y, start_x + self.overlap:end_x] = image[:, self.overlap:]

        if blend_top and not blend_left:
            for i, alpha in enumerate(list(np.linspace(0, 1, self.overlap))):
                self.data[start_y + i, start_x:end_x] = image[i, :] * alpha + self.data[start_y + i, start_x:end_x] * (1 - alpha)
            self.data[start_y + self.overlap:end_y, start_x:end_x] = image[self.overlap:, :]

        if blend_left and blend_top:
            for i, alpha in enumerate(list(np.linspace(0, 1, self.overlap))):
                self.data[start_y + self.overlap:end_y, start_x + i] = image[self.overlap:, i] * alpha + self.data[start_y + self.overlap:end_y, start_x + i] * (1 - alpha)
            self.data[start_y + self.overlap:end_y, start_x + self.overlap:end_x] = image[self.overlap:, self.overlap:]

            for i, alpha in enumerate(list(np.linspace(0, 1, self.overlap))):
                self.data[start_y + i, start_x + self.overlap:end_x] = image[i, self.overlap:] * alpha + self.data[start_y + i, start_x + self.overlap:end_x] * (1 - alpha)
            self.data[start_y + self.overlap:end_y, start_x + self.overlap:end_x] = image[self.overlap:, self.overlap:]

    def right_distance(self, left, right):
        roi = np.array(left[:, -self.overlap:] - right[:, :self.overlap])
        roi = np.abs(roi)
        roi = np.matmul(roi, self.right_kernel)
        return np.amax(np.abs(roi))

    def bottom_distance(self, top, bottom):
        roi = np.array(top[-self.overlap:, :] - bottom[:self.overlap, :])
        roi = np.abs(roi)
        roi = np.transpose(roi)
        roi = np.matmul(roi, self.right_kernel)
        return np.amax(np.abs(roi))

    def best_match(self, images, used, left_index=None, top_index=None):
        assert left_index is not None or top_index is not None
        index = None
        min_diff = None
        list_diff = None

        for idx in range(images.shape[0]):
            if idx not in (left_index, top_index) and idx not in used:
                image = images[idx, :, :]

                if left_index is not None and top_index is None:
                    left = images[left_index, :, :]
                    diff = self.right_distance(left, image)

                elif top_index is not None and left_index is None:
                    top = images[top_index, :, :]
                    diff = self.bottom_distance(top, image)

                else:
                    left = images[left_index, :, :]
                    top = images[top_index, :, :]
                    list_diff = [self.bottom_distance(top, image), self.right_distance(left, image)]
                    diff = np.max([self.bottom_distance(top, image), self.right_distance(left, image)])
                    # diff = self.bottom_distance(top, image) * self.right_distance(left, image)

                if min_diff is None or diff < min_diff:
                    min_diff = diff
                    index = idx

        # s = 'Right' if top_index is None else ('Bottom' if left_index is None else 'RB')
        # print(min_diff, s)
        return index, min_diff if list_diff is None else list_diff

    def fill_best(self, images):
        assert len(images) >= (self.rows * self.cols)

        avg_diff = []
        used = set()
        pbar = tqdm(total=self.rows * self.cols)
        for row in range(self.rows):
            for col in range(self.cols):
                if row == 0 and col == 0:
                    index = random.randrange(len(images))
                    self.set_tile(col, row, images, index)
                elif row == 0:
                    left_index = self.index[row, col - 1]
                    index, diff = self.best_match(images, used, left_index=left_index)
                    self.set_tile(col, row, images, index, blend_left=True)
                    avg_diff.append(diff)
                elif col == 0:
                    top_index = self.index[row - 1, col]
                    index, diff = self.best_match(images, used, top_index=top_index)
                    self.set_tile(col, row, images, index, blend_top=True)
                    avg_diff.append(diff)
                else:
                    left_index = self.index[row, col - 1]
                    top_index = self.index[row - 1, col]
                    index, diff = self.best_match(images, used, left_index=left_index, top_index=top_index)
                    self.set_tile(col, row, images, index, blend_top=True, blend_left=True)
                    avg_diff.extend(diff)

                used.add(index)
                pbar.update(1)

        pbar.close()
        return self, avg_diff

    def fill(self, images):
        self.data.fill(0)
        left = images[0]
        right = None
        min_diff = None

        import time
        total = 0
        count = 0
        for i, image in enumerate(images):
            t0 = time.clock()
            overlap = np.array(left[64:, -5:] - image[64:, :5])
            diff = np.amax(np.abs(overlap))
            t1 = time.clock()
            total += t1 - t0
            count += 1
            print(t1 - t0)

            if min_diff is None or diff < min_diff:
                min_diff = diff
                right = image

        print(total / count)

        left = np.array(left)
        right = np.array(right)
        blend = np.zeros(shape=(128, 10))
        step = 1.0 / 12
        alpha = 1.0 - step
        for i in range(10):
            print(alpha, 1 - alpha)
            blend[:, i] = left[:, (i - 10)] * alpha + right[:, i] * (1 - alpha)
            alpha -= step

        img = np.zeros(shape=(128, 10 * 2))
        img[:, :10] = left[:, -10:]
        img[:, 10:] = right[:, :10]
        plt.subplot(131)
        plt.imshow(img, vmin=-1, vmax=1)
        plt.subplot(132)
        plt.imshow(blend, vmin=-1, vmax=1)
        plt.subplot(133)
        plt.imshow(left[:, -10:] - right[:, :10], vmin=-1, vmax=1)
        plt.show()

        img = np.zeros(shape=(128, 128 * 2))
        img[:, :128] = left
        img[:, 128:] = right
        plt.subplot(211)
        plt.imshow(img, vmin=-1, vmax=1)

        img = np.zeros(shape=(128, 128 * 2 - 10))
        img[:, :(128-10)] = left[:, :-10]
        img[:, (128-10):128] = blend
        img[:, 128:] = right[:, 10:]
        plt.subplot(212)
        plt.imshow(img, vmin=-1, vmax=1)
        plt.show()

        return self

    def iterate(self):
        """ Return tuples of x, y in range of cols and rows. """
        for x in range(self.cols):
            for y in range(self.rows):
                yield x, y

    def __len__(self):
        return self.rows * self.cols
