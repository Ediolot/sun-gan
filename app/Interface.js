
class Interface {
    constructor() {
        this.base_url = 'http://localhost:5050/';
        this.loading = false;
    }

    make_post_init(data) {
        return {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        }
    }

    load_samples(path) {
        if (this.loading)
            return;
        this.loading = true;

        const url =  this.base_url + 'load_samples';
        fetch(url, this.make_post_init(path))
        .then(res => res.json())
        .then(json => {
            this.loading = false;
            console.log('Loaded ' + json.data + ' samples')
        })
        .catch(e => {
            console.error(e);
            this.loading = false;
        });
    }

    load_model(path) {
        if (this.loading)
            return;
        this.loading = true;

        const url =  this.base_url + 'load_model';
        fetch(url, this.make_post_init(path))
            .then(res => res.json())
            .then(json => {
                this.loading = false;
                console.log('Loaded ' + json.data + ' model')
            })
            .catch(e => {
                console.error(e);
                this.loading = false;
            });
    }

    gen_samples(number) {
        if (this.loading)
            return;
        this.loading = true;

        const url =  this.base_url + 'gen_samples';
        fetch(url, this.make_post_init(number))
            .then(res => res.json())
            .then(json => {
                this.loading = false;
                console.log('Generated ' + json.data + ' samples')
            })
            .catch(e => {
                console.error(e);
                this.loading = false;
            });
    }

    gen_image(size, overlap, colormap) {
        if (this.loading)
            return;
        this.loading = true;

        console.log(size, overlap);
        const url =  this.base_url + 'gen_image';
        fetch(url, this.make_post_init([size, overlap, colormap]))
            .then(res => res.json())
            .then(json => {
                this.loading = false;
                imageArea.updateImage();
                console.log('Done')
            })
            .catch(e => {
                console.error(e);
                this.loading = false;
            });
    }

    updateColormap(colormap) {
        console.log(colormap);
        if (this.loading)
            return;
        this.loading = true;

        console.log(colormap);
        const url =  this.base_url + 'recolor';
        fetch(url, this.make_post_init(colormap))
            .then(res => res.json())
            .then(json => {
                this.loading = false;
                imageArea.updateImage();
            })
            .catch(e => {
                console.error(e);
                this.loading = false;
            });
    }
}