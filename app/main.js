const { app, BrowserWindow } = require('electron');

let win;

function main() {
    app.on('ready', createWindow);
    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit()
        }
    });
    app.on('activate', () => {
        if (win === null) {
            createWindow()
        }
    });
}

function createWindow () {
    win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });
    win.loadFile('index.html');
    win.webContents.openDevTools();
    win.removeMenu();
    win.on('closed', () => win = null);
}

main();
