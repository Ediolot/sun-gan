
class ImageArea {
    constructor(element, container, zoomPower=2) {
        this.element = element;
        this.baseSizeX = element.clientWidth;
        this.baseSizeY = element.clientHeight;
        this.offsetX = 0;
        this.offsetY = 0;
        this.zoomVal = 1;
        this.zoomPower = zoomPower;

        element.setAttribute('draggable', false);
        element.style.position = 'relative';
        element.style.left = '50%';
        element.style.top = '50%';

        element.onload = () => this.update();
        container.onresize = () => this.update();
        container.onmousemove = e => this.onMousemove(e);
        container.onmousewheel = e => this.onMousewheel(e);
        this.update();
    }

    updateImage() {
        this.element.src = "../data/generated/buffer.png?" + new Date().getTime();
    }

    onMousemove(e) {
        if (e.buttons & 0x1)
            this.move(e.movementX, e.movementY);
    }

    onMousewheel(e) {
        if (e.deltaY !== 0) {
            this.zoom(Math.sign(e.deltaY));
        }
    }

    zoom(val) {
        this.zoomVal *= val < 0 ? this.zoomPower : 1.0 / this.zoomPower;
        this.update();
    }

    move(x, y) {
        this.offsetX -= x / this.zoomVal;
        this.offsetY -= y / this.zoomVal;
        this.update();
    }

    update() {
        let w = this.baseSizeX * this.zoomVal;
        let h = this.baseSizeY * this.zoomVal;
        let centerX = Math.round(w / 2) + this.offsetX * this.zoomVal;
        let centerY = Math.round(h / 2) + this.offsetY * this.zoomVal;

        this.element.style.width      = w + 'px';
        this.element.style.height     = h + 'px';
        this.element.style.marginLeft = -centerX + 'px';
        this.element.style.marginTop  = -centerY + 'px';
    }
}