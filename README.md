# SunGAN

This project utilizes a Generative Adversarial Network in order to generate images from the Sun surface.

**Abstract**

*This works presents an alternative solution to the already existing physical
simulations for the Sun’s photosphere which are used to output a single texture image from the Sun’s surface. This simulations are slow and require huge
computational power, a cheaper alternative in time and resources is introduced.*

*Architectures for a Generative Adversarial Network and a Variational Autoeconder are considered and trained in the generation of small image tiles
(128x128px) that resemble the surface of the Sun around an area of approximately 2.62 ∗ 107km2 which with the proposed method can be composed into an
image of arbitrarily any size given enough tiles. [...]*

[<img src="/doc/media/8x8.png" width="500" height="500">](/doc/media/8x8.png)

Sample generated Sun surface image

## Work in progress

The code is ready to generate images (see `src/example.py`) of the Sun surface, however as it is being rewritten, the GA that improves the tiling process to create a complete image is not functional at this point as well as the desktop application which are both in progress.


## Repository organization

    |--app/  # Desktop application
    |
    |--data/
    |    |--generated/      # Complete output images
    |    |--generated_aux/  # Generated individual tiles (in .npy format) and generator vectors
    |    |--models/         # Generator and discriminator trained networks
    |    |--simulations/    # Original image data for training
    |  
    |--doc/  # Report
    |
    |--src/
         |--networks/  # Python scripts for the network architectures
         |    |--gan/
         |    |    |--use_networks.py  # Generate image tiles into data/generated_aux
         |    |    
         |    |--other/
         |    |--vae/
         |
         |--other/          # Utility and other python files
         |--preprocessing/  # Python files to process training images
         |--server/         # Server side for the app
         |--tiler/          # Python scripts to join together the generated tiles into an image
         |--example.py      # Python script that generates a complete image from the tiles in data/generated_aux as a commented example
